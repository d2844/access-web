/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/31/22, 11:34 PM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.controller;

import com.sinaungoding.accessweb.dto.Device;
import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.dto.paging.Page;
import com.sinaungoding.accessweb.dto.paging.PagingRequest;
import com.sinaungoding.accessweb.service.DeviceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("devices")
@Slf4j
@AllArgsConstructor
public class DeviceRestController {
    private final DeviceService client;

    @PostMapping
    public Page<Device> list(@RequestBody PagingRequest pagingRequest, HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            log.info("RESPONSE {}", jwtResponse == null ? "JWT NULL" : jwtResponse.getUsername());
            return client.getDevices(pagingRequest, jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }
}
