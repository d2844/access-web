/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 9:19 PM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.controller;

import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.repository.AccessRestClient;
import com.sinaungoding.accessweb.repository.DevicesRestClient;
import com.sinaungoding.accessweb.repository.EmployeesRestClient;
import com.sinaungoding.accessweb.repository.PersoRestClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@Slf4j
@AllArgsConstructor
public class DefaultController {

    private final AccessRestClient accessRestClient;
    private final EmployeesRestClient employeesRestClient;
    private final DevicesRestClient deviceRestClient;
    private final PersoRestClient persoRestClient;

    @GetMapping("/")
    public String index(HttpServletRequest request, ModelMap model) {
        JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
        Long count = accessRestClient.countByDesc("Normal access", jwtResponse.getToken());
        Long employees = employeesRestClient.count(jwtResponse.getToken());
        Long devices = deviceRestClient.count(jwtResponse.getToken());
        Long persos = persoRestClient.count(jwtResponse.getToken());
        model.addAttribute("totalAcc", count);
        model.addAttribute("totalEmp", employees);
        model.addAttribute("totalDev", devices);
        model.addAttribute("totalPers", persos);
        return "index";
    }

    @GetMapping("/login")
    public String login() throws Exception {
        return "login";
    }

}
