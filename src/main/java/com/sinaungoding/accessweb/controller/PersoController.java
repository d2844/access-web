/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 6:14 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.dto.Perso;
import com.sinaungoding.accessweb.repository.PersoRestClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Objects;

@Controller
@Slf4j
@RequestMapping("/perso")
@AllArgsConstructor
public class PersoController {
    private final PersoRestClient client;
    private final ObjectMapper objectMapper;

    @GetMapping("/list")
    public String findAll() {
        return "/perso/perso-list";
    }

    @GetMapping
    public String showAdd(ModelMap model) {
        model.addAttribute("perso", new Perso());
        return "/perso/perso-add";
    }

    @GetMapping("/{id}")
    public String showEdit(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Perso perso = client.findById(id, jwtResponse.getToken());
            modelMap.addAttribute("perso", perso);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "/perso/perso-edit";
    }

    @GetMapping("/list/{id}")
    public String showDetail(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Perso perso = client.findById(id, jwtResponse.getToken());
            log.info(perso.toString());
            modelMap.addAttribute("perso", perso);
        } catch (HttpStatusCodeException e) {
            log.error("Code {} Message {}", e.getStatusCode().value(), e.getResponseBodyAsString());
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        return "/perso/perso-detail";
    }

    @PostMapping("/add")
    public String addEmployee(@ModelAttribute @Valid Perso perso, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            log.warn("found error {}", errors.getObjectName());
            return "/perso/perso-add";
        }
        try {
            log.info(objectMapper.writeValueAsString(perso));
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            ResponseEntity<String> save = client.save(perso, jwtResponse.getToken());
            return "/perso/perso-list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/perso/perso-add";
    }

    @PostMapping("/edit/{id}")
    public String editEmployee(@PathVariable("id") String id, @ModelAttribute @Valid Perso perso, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            perso.setId(id);
            log.warn("found error {}", errors.getObjectName());
            return "/perso/perso-edit";
        }
        try {
            log.info(objectMapper.writeValueAsString(perso));
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            ResponseEntity<String> update = client.update(perso, jwtResponse.getToken());
            return "/perso/perso-list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/perso/perso-edit";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable("id") String id, HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Perso perso = client.findById(id, jwtResponse.getToken());
            log.info(perso.toString());
            client.delete(id, jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "redirect:/employee/list";
    }
}
