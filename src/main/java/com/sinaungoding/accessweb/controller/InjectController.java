/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 6:14 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinaungoding.accessweb.dto.*;
import com.sinaungoding.accessweb.repository.DevicesRestClient;
import com.sinaungoding.accessweb.repository.InjectRestClient;
import com.sinaungoding.accessweb.repository.PersoRestClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.Objects;

@Controller
@Slf4j
@RequestMapping("/inject")
@AllArgsConstructor
public class InjectController {
    private final InjectRestClient client;
    private final DevicesRestClient clientDev;
    private final PersoRestClient clientPerso;
    private final ObjectMapper objectMapper;

    @GetMapping("/list")
    public String findAll(ModelMap model, HttpServletRequest request) {
        JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
        Page<Device> all = clientDev.findAll(String.format("pageSize=%d", Integer.MAX_VALUE), jwtResponse.getToken());
        Page<Perso> persoAll = clientPerso.findAll(String.format("pageSize=%d", Integer.MAX_VALUE), jwtResponse.getToken());
        model.put("devices", all.getContent());
        model.put("persos", persoAll.getContent());
        model.put("inject", new InjectDT());
        return "/perso/inject-list";
    }

    @GetMapping
    public String showAdd(ModelMap model) {
        model.addAttribute("inject", new Inject());
        return "/perso/inject-add";
    }

    @GetMapping("/{id}")
    public String showEdit(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Inject inject = client.findById(id, jwtResponse.getToken());
            modelMap.addAttribute("inject", inject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "/perso/inject-edit";
    }

    @GetMapping("/list/{id}")
    public String showDetail(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Inject inject = client.findById(id, jwtResponse.getToken());
            log.info(inject.toString());
            modelMap.addAttribute("inject", inject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "/perso/inject-detail";
    }

    @PostMapping("/add")
    public String addEmployee(@ModelAttribute @Valid InjectDT inject, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            log.warn("found error {}", errors.getObjectName());
            return "/perso/inject-add";
        }
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            log.info(Arrays.toString(inject.getPersos()));
            log.info(Arrays.toString(inject.getDevices()));
            log.info(objectMapper.writeValueAsString(inject));
            ResponseEntity<String> save = client.save(inject, jwtResponse.getToken());
            return "redirect:/inject/list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/perso/inject-add";
    }

    @PostMapping("/edit/{id}")
    public String editEmployee(@PathVariable("id") String id, @ModelAttribute @Valid Inject inject, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            inject.setId(id);
            log.warn("found error {}", errors.getObjectName());
            return "/perso/inject-edit";
        }
        try {
            log.info(objectMapper.writeValueAsString(inject));
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            ResponseEntity<String> update = client.update(inject, jwtResponse.getToken());
            return "/perso/inject-list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/perso/inject-edit";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable("id") String id, HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Inject inject = client.findById(id, jwtResponse.getToken());
            log.info(inject.toString());
            client.delete(id, jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "redirect:/inject/list";
    }
}
