/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 6:14 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinaungoding.accessweb.dto.Device;
import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.repository.DevicesRestClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Objects;

@Controller
@Slf4j
@RequestMapping("/device")
@AllArgsConstructor
public class DevicesController {
    private final DevicesRestClient client;
    private final ObjectMapper objectMapper;

    @GetMapping("/list")
    public String findAll() {
        return "/device/device-list";
    }

    @GetMapping
    public String showAdd(ModelMap model) {
        model.addAttribute("device", new Device());
        return "/device/device-add";
    }

    @GetMapping("/{id}")
    public String showEdit(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Device device = client.findById(id, jwtResponse.getToken());
            modelMap.addAttribute("device", device);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "/device/device-edit";
    }

    @GetMapping("/list/{id}")
    public String showDetail(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Device device = client.findById(id, jwtResponse.getToken());
            log.info(device.toString());
            modelMap.addAttribute("device", device);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "/device/device-detail";
    }

    @PostMapping("/add")
    public String addDevice(@ModelAttribute @Valid Device device, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            log.warn("found error {}", errors.getObjectName());
            return "/device/device-add";
        }
        try {
            log.info(objectMapper.writeValueAsString(device));
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            ResponseEntity<String> save = client.save(device, jwtResponse.getToken());
            return "/device/device-list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/device/device-add";
    }

    @PostMapping("/edit/{id}")
    public String editDevice(@PathVariable("id") String id, @ModelAttribute @Valid Device device, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            device.setId(id);
            log.warn("found error {}", errors.getObjectName());
            return "/device/device-edit";
        }
        try {
            log.info(objectMapper.writeValueAsString(device));
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            ResponseEntity<String> update = client.update(device, jwtResponse.getToken());
            return "/device/device-list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/device/device-edit";
    }

    @GetMapping("/delete/{id}")
    public String deleteDevice(@PathVariable("id") String id, HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Device device = client.findById(id, jwtResponse.getToken());
            log.info(device.toString());
            client.delete(id, jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "redirect:/device/list";
    }
}
