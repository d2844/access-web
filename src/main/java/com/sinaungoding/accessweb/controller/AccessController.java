/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 6:14 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinaungoding.accessweb.dto.Access;
import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.repository.AccessRestClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Objects;

@Controller
@Slf4j
@RequestMapping("/access")
@AllArgsConstructor
public class AccessController {
    private final AccessRestClient client;
    private final ObjectMapper objectMapper;

    @GetMapping("/list")
    public String findAll() {
        return "/perso/access-list";
    }

    @GetMapping
    public String showAdd(ModelMap model) {
        model.addAttribute("access", new Access());
        return "/perso/access-add";
    }

    @GetMapping("/{id}")
    public String showEdit(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Access access = client.findById(id, jwtResponse.getToken());
            modelMap.addAttribute("access", access);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "/perso/access-edit";
    }

    @GetMapping("/list/{id}")
    public String showDetail(@PathVariable String id, HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Access access = client.findById(id, jwtResponse.getToken());
            log.info(access.toString());
            modelMap.addAttribute("access", access);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "/perso/access-detail";
    }

    @PostMapping("/add")
    public String addEmployee(@ModelAttribute @Valid Access access, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            log.warn("found error {}", errors.getObjectName());
            return "/perso/access-add";
        }
        try {
            log.info(objectMapper.writeValueAsString(access));
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            ResponseEntity<String> save = client.save(access, jwtResponse.getToken());
            return "/perso/access-list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/perso/access-add";
    }

    @PostMapping("/edit/{id}")
    public String editEmployee(@PathVariable("id") String id, @ModelAttribute @Valid Access access, HttpServletRequest request, BindingResult errors) throws Exception {
        if (errors.hasErrors()) {
            access.setId(id);
            log.warn("found error {}", errors.getObjectName());
            return "/perso/access-edit";
        }
        try {
            log.info(objectMapper.writeValueAsString(access));
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            ResponseEntity<String> update = client.update(access, jwtResponse.getToken());
            return "/perso/access-list";
        } catch (HttpStatusCodeException e) {
            log.error(e.getMessage());
            ResponseEntity<String> response = ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
            errors.reject("error.object", Objects.requireNonNull(response.getBody()));
        } catch (Exception e) {
            log.error(e.getMessage());
            errors.reject("error.object", e.getMessage());
        }
        return "/perso/access-edit";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable("id") String id, HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            Access access = client.findById(id, jwtResponse.getToken());
            log.info(access.toString());
            client.delete(id, jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return "redirect:/access/list";
    }
}
