/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 6/30/22, 12:05 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.controller;

import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.dto.groupBy.GroupByCard;
import com.sinaungoding.accessweb.dto.groupBy.GroupByDevice;
import com.sinaungoding.accessweb.dto.groupBy.GroupByTanggal;
import com.sinaungoding.accessweb.repository.DashboardRestClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("dashboard")
@Slf4j
@AllArgsConstructor
public class DefaultRestController {
    private final DashboardRestClient client;

    @GetMapping("/byDevice")
    public List<GroupByDevice> findAllByDevice(HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            return client.findAllByDevice(jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping("/byCard")
    public List<GroupByCard> findAllByCard(HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            return client.findAllByCard(jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping("/byDate")
    public List<GroupByTanggal> findAllByDate(HttpServletRequest request) throws Exception {
        try {
            JwtResponse jwtResponse = (JwtResponse) request.getSession().getAttribute("jwt");
            return client.findAllByDate(jwtResponse.getToken());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }
}
