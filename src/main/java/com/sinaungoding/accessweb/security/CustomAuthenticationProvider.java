/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/28/22, 8:32 PM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.security;

import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.repository.AuthRestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private AuthRestClient client;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
        final String username = token.getName();
        final String password = (String) token.getCredentials();
        if (username == null || password == null)
            throw new BadCredentialsException("Invalid Login Credentials");
        try {
            JwtResponse jwtResponse = client.login(username, password);
            log.info("Token         :" + jwtResponse.getToken());
            log.info("Refresh token :" + jwtResponse.getRefreshToken());
            List<GrantedAuthority> authorities = new ArrayList<>();
            jwtResponse.getRoles().forEach(role -> {
                authorities.add(new SimpleGrantedAuthority(role));
            });
//            List<GrantedAuthority> authorities = user.getRoles().stream()
//                    .map(role -> new SimpleGrantedAuthority(role.getName().name()))
//                    .collect(Collectors.toList());
//            UserDetails user = new User(username, password, authorities);
//            String jwt = null;
//            try {
//                jwt = objectMapper.writeValueAsString(jwtResponse);
//            } catch (JsonProcessingException e) {
//                log.error(e.getMessage(), e);
//            }
            return new UsernamePasswordAuthenticationToken(jwtResponse, password, authorities);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadCredentialsException("Invalid Login Credentials");
        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
