package com.sinaungoding.accessweb.service;

import com.sinaungoding.accessweb.dto.Device;
import com.sinaungoding.accessweb.dto.DeviceComparators;
import com.sinaungoding.accessweb.dto.paging.Column;
import com.sinaungoding.accessweb.dto.paging.Order;
import com.sinaungoding.accessweb.dto.paging.Page;
import com.sinaungoding.accessweb.dto.paging.PagingRequest;
import com.sinaungoding.accessweb.repository.DevicesRestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-bootstrap-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/11/20
 * Time: 13.42
 */
@Slf4j
@Service
public class DeviceService {
    @Autowired
    private DevicesRestClient client;
    private static final Comparator<Device> EMPTY_COMPARATOR = (e1, e2) -> 0;

    public Page<Device> getDevices(PagingRequest pagingRequest, String token) {
        org.springframework.data.domain.Page<Device> devices = client.findAll(String.format("pageSize=%d", Integer.MAX_VALUE), token);
        log.info("Result    :{}", devices == null ? 0 : devices.getTotalElements());
        return getPage(devices.getContent(), pagingRequest);
    }

    private Page<Device> getPage(List<Device> employees, PagingRequest pagingRequest) {
        List<Device> filtered = employees.stream()
                .sorted(sortEmployees(pagingRequest))
                .filter(filterEmployees(pagingRequest))
                .skip(pagingRequest.getStart())
                .limit(pagingRequest.getLength())
                .collect(Collectors.toList());

        long count = employees.stream()
                .filter(filterEmployees(pagingRequest))
                .count();

        Page<Device> page = new Page<>(filtered);
        page.setRecordsFiltered((int) count);
        page.setRecordsTotal((int) count);
        page.setDraw(pagingRequest.getDraw());

        return page;
    }

    private Predicate<Device> filterEmployees(PagingRequest pagingRequest) {
        if (pagingRequest.getSearch() == null || StringUtils.isEmpty(pagingRequest.getSearch()
                .getValue())) {
            return employee -> true;
        }

        String value = pagingRequest.getSearch()
                .getValue();

        return employee -> employee.getId()
                .toLowerCase()
                .contains(value)
                || employee.getName()
                .toLowerCase()
                .contains(value)
                || employee.getIpAddress()
                .toLowerCase()
                .contains(value);
    }

    private Comparator<Device> sortEmployees(PagingRequest pagingRequest) {
        if (pagingRequest.getOrder() == null) {
            return EMPTY_COMPARATOR;
        }

        try {
            Order order = pagingRequest.getOrder()
                    .get(0);

            int columnIndex = order.getColumn();
            Column column = pagingRequest.getColumns()
                    .get(columnIndex);

            Comparator<Device> comparator = DeviceComparators.getComparator(column.getData(), order.getDir());
            if (comparator == null) {
                return EMPTY_COMPARATOR;
            }

            return comparator;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return EMPTY_COMPARATOR;
    }
}
