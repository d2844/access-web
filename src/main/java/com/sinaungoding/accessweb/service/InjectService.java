package com.sinaungoding.accessweb.service;

import com.sinaungoding.accessweb.dto.Inject;
import com.sinaungoding.accessweb.dto.InjectComparators;
import com.sinaungoding.accessweb.dto.paging.Column;
import com.sinaungoding.accessweb.dto.paging.Order;
import com.sinaungoding.accessweb.dto.paging.Page;
import com.sinaungoding.accessweb.dto.paging.PagingRequest;
import com.sinaungoding.accessweb.repository.InjectRestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-bootstrap-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/11/20
 * Time: 13.42
 */
@Slf4j
@Service
public class InjectService {
    @Autowired
    private InjectRestClient client;
    private static final Comparator<Inject> EMPTY_COMPARATOR = (e1, e2) -> 0;

    public Page<Inject> getInjects(PagingRequest pagingRequest, String token) {
        org.springframework.data.domain.Page<Inject> injects = client.findAll(String.format("pageSize=%d", Integer.MAX_VALUE), token);
        log.info("Result    :{}", injects == null ? 0 : injects.getTotalElements());
        return getPage(injects.getContent(), pagingRequest);
    }

    private Page<Inject> getPage(List<Inject> employees, PagingRequest pagingRequest) {
        List<Inject> filtered = employees.stream()
                .sorted(sortEmployees(pagingRequest))
                .filter(filterEmployees(pagingRequest))
                .skip(pagingRequest.getStart())
                .limit(pagingRequest.getLength())
                .collect(Collectors.toList());

        long count = employees.stream()
                .filter(filterEmployees(pagingRequest))
                .count();

        Page<Inject> page = new Page<>(filtered);
        page.setRecordsFiltered((int) count);
        page.setRecordsTotal((int) count);
        page.setDraw(pagingRequest.getDraw());

        return page;
    }

    private Predicate<Inject> filterEmployees(PagingRequest pagingRequest) {
        if (pagingRequest.getSearch() == null || StringUtils.isEmpty(pagingRequest.getSearch()
                .getValue())) {
            return employee -> true;
        }

        String value = pagingRequest.getSearch()
                .getValue();

        return inject -> inject.getPerso().getSerial()
                .toLowerCase()
                .contains(value)
                || inject.getPerso().getEmployee().getFirstName()
                .toLowerCase()
                .contains(value)
                || inject.getPerso().getEmployee().getLastName()
                .toLowerCase()
                .contains(value)
                || inject.getDevice().getName()
                .toLowerCase()
                .contains(value)
                || inject.getDevice().getIpAddress()
                .toLowerCase()
                .contains(value);
    }

    private Comparator<Inject> sortEmployees(PagingRequest pagingRequest) {
        if (pagingRequest.getOrder() == null) {
            return EMPTY_COMPARATOR;
        }

        try {
            Order order = pagingRequest.getOrder()
                    .get(0);

            int columnIndex = order.getColumn();
            Column column = pagingRequest.getColumns()
                    .get(columnIndex);

            Comparator<Inject> comparator = InjectComparators.getComparator(column.getData(), order.getDir());
            if (comparator == null) {
                return EMPTY_COMPARATOR;
            }

            return comparator;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return EMPTY_COMPARATOR;
    }
}
