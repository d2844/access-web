package com.sinaungoding.accessweb.service;

import com.sinaungoding.accessweb.dto.Employee;
import com.sinaungoding.accessweb.dto.EmployeeComparators;
import com.sinaungoding.accessweb.dto.paging.Column;
import com.sinaungoding.accessweb.dto.paging.Order;
import com.sinaungoding.accessweb.dto.paging.Page;
import com.sinaungoding.accessweb.dto.paging.PagingRequest;
import com.sinaungoding.accessweb.repository.EmployeesRestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-bootstrap-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/11/20
 * Time: 13.42
 */
@Slf4j
@Service
public class EmployeeService {
    @Autowired
    private EmployeesRestClient client;
    private static final Comparator<Employee> EMPTY_COMPARATOR = (e1, e2) -> 0;

    public Page<Employee> getEmployees(PagingRequest pagingRequest, String token) {
        org.springframework.data.domain.Page<Employee> employees = client.findAll(String.format("pageSize=%d", Integer.MAX_VALUE), token);
        log.info("Result    :{}", employees == null ? 0 : employees.getTotalElements());
        return getPage(employees.getContent(), pagingRequest);
    }

    private Page<Employee> getPage(List<Employee> employees, PagingRequest pagingRequest) {
        List<Employee> filtered = employees.stream()
                .sorted(sortEmployees(pagingRequest))
                .filter(filterEmployees(pagingRequest))
                .skip(pagingRequest.getStart())
                .limit(pagingRequest.getLength())
                .collect(Collectors.toList());

        long count = employees.stream()
                .filter(filterEmployees(pagingRequest))
                .count();

        Page<Employee> page = new Page<>(filtered);
        page.setRecordsFiltered((int) count);
        page.setRecordsTotal((int) count);
        page.setDraw(pagingRequest.getDraw());

        return page;
    }

    private Predicate<Employee> filterEmployees(PagingRequest pagingRequest) {
        if (pagingRequest.getSearch() == null || StringUtils.isEmpty(pagingRequest.getSearch()
                .getValue())) {
            return employee -> true;
        }

        String value = pagingRequest.getSearch()
                .getValue();

        return employee -> employee.getEmpNo()
                .toLowerCase()
                .contains(value)
                || employee.getFirstName()
                .toLowerCase()
                .contains(value)
                || employee.getLastName()
                .toLowerCase()
                .contains(value);
    }

    private Comparator<Employee> sortEmployees(PagingRequest pagingRequest) {
        if (pagingRequest.getOrder() == null) {
            return EMPTY_COMPARATOR;
        }

        try {
            Order order = pagingRequest.getOrder()
                    .get(0);

            int columnIndex = order.getColumn();
            Column column = pagingRequest.getColumns()
                    .get(columnIndex);

            Comparator<Employee> comparator = EmployeeComparators.getComparator(column.getData(), order.getDir());
            if (comparator == null) {
                return EMPTY_COMPARATOR;
            }

            return comparator;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return EMPTY_COMPARATOR;
    }
}
