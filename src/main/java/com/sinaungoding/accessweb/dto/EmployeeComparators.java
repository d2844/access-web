package com.sinaungoding.accessweb.dto;

import com.sinaungoding.accessweb.dto.paging.Direction;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-bootstrap-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/11/20
 * Time: 13.40
 */
public final class EmployeeComparators {

    @EqualsAndHashCode
    @AllArgsConstructor
    @Getter
    static class Key {
        String name;
        Direction dir;
    }

    static Map<Key, Comparator<Employee>> map = new HashMap<>();

    static {
        map.put(new Key("empNo", Direction.asc), Comparator.comparing(Employee::getEmpNo));
        map.put(new Key("empNo", Direction.desc), Comparator.comparing(Employee::getEmpNo)
                .reversed());

        map.put(new Key("firstName", Direction.asc), Comparator.comparing(Employee::getFirstName));
        map.put(new Key("firstName", Direction.desc), Comparator.comparing(Employee::getFirstName)
                .reversed());

        map.put(new Key("lastName", Direction.asc), Comparator.comparing(Employee::getLastName));
        map.put(new Key("lastName", Direction.desc), Comparator.comparing(Employee::getLastName)
                .reversed());

        map.put(new Key("gender", Direction.asc), Comparator.comparing(Employee::getGender));
        map.put(new Key("gender", Direction.desc), Comparator.comparing(Employee::getGender)
                .reversed());

        map.put(new Key("birthDate", Direction.asc), Comparator.comparing(Employee::getBirthDate));
        map.put(new Key("birthDate", Direction.desc), Comparator.comparing(Employee::getBirthDate)
                .reversed());

        map.put(new Key("hireDate", Direction.asc), Comparator.comparing(Employee::getHireDate));
        map.put(new Key("hireDate", Direction.desc), Comparator.comparing(Employee::getHireDate)
                .reversed());

    }

    public static Comparator<Employee> getComparator(String name, Direction dir) {
        return map.get(new Key(name, dir));
    }

    private EmployeeComparators() {
    }
}
