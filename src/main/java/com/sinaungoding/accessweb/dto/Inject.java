/*
 * access-service
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 4/28/22, 12:58 PM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Inject extends AbstractEntityDto {
    private String id;
    private Perso perso;
    private Device device;
    private int address;
    private boolean success;
}
