package com.sinaungoding.accessweb.dto;

import com.sinaungoding.accessweb.dto.paging.Direction;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-bootstrap-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/11/20
 * Time: 13.40
 */
public final class PersoComparators {

    @EqualsAndHashCode
    @AllArgsConstructor
    @Getter
    static class Key {
        String name;
        Direction dir;
    }

    static Map<Key, Comparator<Perso>> map = new HashMap<>();

    static {

        map.put(new Key("serial", Direction.asc), Comparator.comparing(Perso::getSerial));
        map.put(new Key("serial", Direction.desc), Comparator.comparing(Perso::getSerial)
                .reversed());

        map.put(new Key("EmpNo", Direction.asc), Comparator.comparing(Perso::getEmployee, (o1, o2) -> o2.getEmpNo().compareTo(o1.getEmpNo())));
        map.put(new Key("EmpNo", Direction.desc), Comparator.comparing(Perso::getEmployee, (o1, o2) -> o2.getEmpNo().compareTo(o1.getEmpNo())).reversed());

        map.put(new Key("firstName", Direction.asc), Comparator.comparing(Perso::getEmployee, (o1, o2) -> o2.getFirstName().compareTo(o1.getFirstName())));
        map.put(new Key("firstName", Direction.desc), Comparator.comparing(Perso::getEmployee, (o1, o2) -> o2.getFirstName().compareTo(o1.getFirstName())).reversed());

        map.put(new Key("lastName", Direction.asc), Comparator.comparing(Perso::getEmployee, (o1, o2) -> o2.getLastName().compareTo(o1.getLastName())));
        map.put(new Key("lastName", Direction.desc), Comparator.comparing(Perso::getEmployee, (o1, o2) -> o2.getLastName().compareTo(o1.getLastName()))
                .reversed());

    }

    public static Comparator<Perso> getComparator(String name, Direction dir) {
        return map.get(new Key(name, dir));
    }

    private PersoComparators() {
    }
}
