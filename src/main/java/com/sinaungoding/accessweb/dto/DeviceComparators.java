package com.sinaungoding.accessweb.dto;

import com.sinaungoding.accessweb.dto.paging.Direction;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-bootstrap-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/11/20
 * Time: 13.40
 */
public final class DeviceComparators {

    @EqualsAndHashCode
    @AllArgsConstructor
    @Getter
    static class Key {
        String name;
        Direction dir;
    }

    static Map<Key, Comparator<Device>> map = new HashMap<>();

    static {
        map.put(new Key("id", Direction.asc), Comparator.comparing(Device::getId));
        map.put(new Key("id", Direction.desc), Comparator.comparing(Device::getId)
                .reversed());

        map.put(new Key("name", Direction.asc), Comparator.comparing(Device::getName));
        map.put(new Key("name", Direction.desc), Comparator.comparing(Device::getName)
                .reversed());

        map.put(new Key("node", Direction.asc), Comparator.comparing(Device::getNode));
        map.put(new Key("node", Direction.desc), Comparator.comparing(Device::getNode)
                .reversed());

        map.put(new Key("ipAddress", Direction.asc), Comparator.comparing(Device::getIpAddress));
        map.put(new Key("ipAddress", Direction.desc), Comparator.comparing(Device::getIpAddress)
                .reversed());

        map.put(new Key("port", Direction.asc), Comparator.comparing(Device::getPort));
        map.put(new Key("port", Direction.desc), Comparator.comparing(Device::getPort)
                .reversed());

    }

    public static Comparator<Device> getComparator(String name, Direction dir) {
        return map.get(new Key(name, dir));
    }

    private DeviceComparators() {
    }
}
