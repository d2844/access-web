package com.sinaungoding.accessweb.dto;

import com.sinaungoding.accessweb.dto.paging.Direction;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-bootstrap-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/11/20
 * Time: 13.40
 */
public final class InjectComparators {

    @EqualsAndHashCode
    @AllArgsConstructor
    @Getter
    static class Key {
        String name;
        Direction dir;
    }

    static Map<Key, Comparator<Inject>> map = new HashMap<>();

    static {

        map.put(new Key("serial", Direction.asc), Comparator.comparing(Inject::getPerso, (o1, o2) -> o2.getSerial().compareTo(o1.getSerial())));
        map.put(new Key("serial", Direction.desc), Comparator.comparing(Inject::getPerso, (o1, o2) -> o2.getSerial().compareTo(o1.getSerial())).reversed());

        map.put(new Key("firstName", Direction.asc), Comparator.comparing(Inject::getPerso, (o1, o2) -> o2.getEmployee().getFirstName().compareTo(o1.getEmployee().getFirstName())));
        map.put(new Key("firstName", Direction.desc), Comparator.comparing(Inject::getPerso, (o1, o2) -> o2.getEmployee().getFirstName().compareTo(o1.getEmployee().getFirstName())).reversed());

        map.put(new Key("lastName", Direction.asc), Comparator.comparing(Inject::getPerso, (o1, o2) -> o2.getEmployee().getLastName().compareTo(o1.getEmployee().getLastName())));
        map.put(new Key("lastName", Direction.desc), Comparator.comparing(Inject::getPerso, (o1, o2) -> o2.getEmployee().getLastName().compareTo(o1.getEmployee().getLastName())).reversed());

        map.put(new Key("deviceName", Direction.asc), Comparator.comparing(Inject::getDevice, (o1, o2) -> o2.getName().compareTo(o1.getName())));
        map.put(new Key("deviceName", Direction.desc), Comparator.comparing(Inject::getDevice, (o1, o2) -> o2.getName().compareTo(o1.getName())).reversed());

        map.put(new Key("deviceIp", Direction.asc), Comparator.comparing(Inject::getDevice, (o1, o2) -> o2.getIpAddress().compareTo(o1.getIpAddress())));
        map.put(new Key("deviceIp", Direction.desc), Comparator.comparing(Inject::getDevice, (o1, o2) -> o2.getIpAddress().compareTo(o1.getIpAddress())).reversed());

    }

    public static Comparator<Inject> getComparator(String name, Direction dir) {
        return map.get(new Key(name, dir));
    }

    private InjectComparators() {
    }
}
