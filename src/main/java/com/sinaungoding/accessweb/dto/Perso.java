/*
 * access-service
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 4/26/22, 11:23 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Perso extends AbstractEntityDto {
    private String id;
    private String serial;
    private Employee employee;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = "Asia/Jakarta")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date expired;
    private boolean active;
}
