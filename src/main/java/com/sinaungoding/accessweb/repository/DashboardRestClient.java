/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 5:43 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.repository;

import com.sinaungoding.accessweb.dto.groupBy.GroupByCard;
import com.sinaungoding.accessweb.dto.groupBy.GroupByDevice;
import com.sinaungoding.accessweb.dto.groupBy.GroupByTanggal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class DashboardRestClient {
    @Autowired
    private RestTemplate template;
    @Value(value = "${server.rest}")
    private String server;

    public List<GroupByDevice> findAllByDevice(String token) throws HttpStatusCodeException {
        try {
            ParameterizedTypeReference<List<GroupByDevice>> reference = new ParameterizedTypeReference<List<GroupByDevice>>() {
            };
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<GroupByDevice> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/dashboard/byDevice", GET, entity, reference).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public List<GroupByTanggal> findAllByDate(String token) throws HttpStatusCodeException {
        try {
            ParameterizedTypeReference<List<GroupByTanggal>> reference = new ParameterizedTypeReference<List<GroupByTanggal>>() {
            };
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<GroupByTanggal> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/dashboard/byDate", GET, entity, reference).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public List<GroupByCard> findAllByCard(String token) throws HttpStatusCodeException {
        try {
            List<GroupByCard> list = new ArrayList<>();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<GroupByCard> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/dashboard/byCard", GET, entity, new ParameterizedTypeReference<List<GroupByCard>>() {
            }).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

}
