/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 5:43 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.repository;

import com.sinaungoding.accessweb.dto.Inject;
import com.sinaungoding.accessweb.dto.InjectDT;
import com.sinaungoding.accessweb.util.RestResponsePage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class InjectRestClient {
    @Autowired
    private RestTemplate template;
    @Value(value = "${server.rest}")
    private String server;

    public ResponseEntity<String> save(Inject dto, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Inject> entity = new HttpEntity<>(dto, headers);
            return template.exchange(server + "/inject", POST, entity, String.class);
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public ResponseEntity<String> save(InjectDT dto, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<InjectDT> entity = new HttpEntity<>(dto, headers);
            return template.exchange(server + "/inject/collection", POST, entity, String.class);
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public ResponseEntity<String> update(Inject dto, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Inject> entity = new HttpEntity<>(dto, headers);
            return template.exchange(server + "/inject/{id}", PATCH, entity, String.class, dto.getId());
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public boolean delete(String id, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Inject> entity = new HttpEntity<>(headers);
            ResponseEntity<String> response = template.exchange(server + "/inject/{id}", DELETE, entity, String.class, id);
            return response.getStatusCode() == OK;
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public Page<Inject> findAll(String params, String token) throws HttpStatusCodeException {
        try {
            ParameterizedTypeReference<RestResponsePage<Inject>> reference = new ParameterizedTypeReference<RestResponsePage<Inject>>() {
            };
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Inject> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/inject" + (params == null ? "" : "?" + params), GET, entity, reference).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public Inject findById(String id, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Inject> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/inject/{id}", GET, entity, Inject.class, id).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }
}
