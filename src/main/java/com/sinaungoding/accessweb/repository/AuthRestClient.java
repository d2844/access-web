/*
 * access-subscriber
 *
 * Copyright (c) 2021
 * All rights reserved
 * Written by od3ng created on 11/7/21, 3:21 PM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.repository;

import com.sinaungoding.accessweb.dto.JwtResponse;
import com.sinaungoding.accessweb.dto.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class AuthRestClient {
    @Autowired
    private RestTemplate template;
    @Value(value = "${server.rest}")
    private String server;

    public User signUp(User dto) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            HttpEntity<User> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<User> response = template.exchange(server + "/auth/signup", POST, entity, User.class);
            return response.getStatusCode() == CREATED ? response.getBody() : null;
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public JwtResponse login(String username, String password) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("username", username);
            map.add("password", password);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            ResponseEntity<JwtResponse> jwtResponseResponseEntity = template.exchange(server + "/auth/login", POST, request, JwtResponse.class);
            if (HttpStatus.OK == jwtResponseResponseEntity.getStatusCode()) {
                JwtResponse body = jwtResponseResponseEntity.getBody();
                log.info(body.toString());
                return body;
            }
        } catch (HttpStatusCodeException e) {
            throw e;
        }
        return null;
    }

    public JwtResponse refresh_token(String refresh_token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + refresh_token);
            HttpEntity<String> request = new HttpEntity<>(headers);
            ResponseEntity<JwtResponse> response = template.exchange(server + "/auth/refresh_token", GET, request, JwtResponse.class);
            if (HttpStatus.OK == response.getStatusCode()) {
                log.info(response.getBody().toString());
                return response.getBody();
            }
        } catch (HttpStatusCodeException e) {
            throw e;
        }
        return null;
    }

}
