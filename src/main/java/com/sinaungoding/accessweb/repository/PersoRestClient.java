/*
 * access-web
 *
 * Copyright (c) 2022
 * All rights reserved
 * Written by od3ng created on 5/27/22, 5:43 AM
 * Blog    : sinaungoding.com
 * Email   : noprianto@polinema.ac.id
 * Github  : 0d3ng
 * Hp      : 085878554150
 */

package com.sinaungoding.accessweb.repository;

import com.sinaungoding.accessweb.dto.Perso;
import com.sinaungoding.accessweb.util.RestResponsePage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class PersoRestClient {
    @Autowired
    private RestTemplate template;
    @Value(value = "${server.rest}")
    private String server;

    public ResponseEntity<String> save(Perso dto, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Perso> entity = new HttpEntity<>(dto, headers);
            return template.exchange(server + "/persos", POST, entity, String.class);
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public ResponseEntity<String> update(Perso dto, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Perso> entity = new HttpEntity<>(dto, headers);
            return template.exchange(server + "/persos/{id}", PATCH, entity, String.class, dto.getId());
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public boolean delete(String id, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Perso> entity = new HttpEntity<>(headers);
            ResponseEntity<String> response = template.exchange(server + "/persos/{id}", DELETE, entity, String.class, id);
            return response.getStatusCode() == OK;
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public Page<Perso> findAll(String params, String token) throws HttpStatusCodeException {
        try {
            ParameterizedTypeReference<RestResponsePage<Perso>> reference = new ParameterizedTypeReference<RestResponsePage<Perso>>() {
            };
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Perso> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/persos" + (params == null ? "" : "?" + params), GET, entity, reference).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public Perso findById(String id, String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Perso> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/persos/{id}", GET, entity, Perso.class, id).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public Long count(String token) throws HttpStatusCodeException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<Long> entity = new HttpEntity<>(headers);
            return template.exchange(server + "/persos/count", GET, entity, Long.class).getBody();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }
}
